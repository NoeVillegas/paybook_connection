var express = require('express');
var app = express();
const axios = require('axios')
var idUser = '5da78bf98c91e75a5557019d';
var apiUrl = 'https://sync.paybook.com/v1/';
var apiKey = '0135fe1925965d9c2d97eb63c5e1967e';
var idSite ='56cf5728784806f72b8b4568';
const headers = {
  'Content-Type': 'application/json',
  'Authorization': `api_key api_key=${apiKey}`
}
var token = '';
var headersBearer = {};
var bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
  res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
  res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
  next();
});

app.get('/', function (req, res) {
  res.send('Hello World!');
});

//function generateSession(){
  app.get('/sessions', function (req, res) {
    axios.post(
      `${apiUrl}sessions`,
      {
        "id_user" : idUser
      },{
        headers
      })
      .then((response) => {
        token = response.data.response.token;
        headersBearer = {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${token}`
        };
        res.send(response.data.response.token);
      })
      .catch((error) => {
        res.send(error)
      })
  });
//}
app.get('/credentials', function (req, res) {
  axios.get(
    `${apiUrl}credentials`,{
      headers: headersBearer
    })
    .then((response) => {
      console.log('****************************************************');
      console.log('SUCCSESS');
      res.send(response.data);
      console.log('****************************************************');
    })
    .catch((error) => {
      console.log('****************************************************');
      console.log('ERROR');
      if(error && error.response && error.response.status){
        console.log(error.response.status);
        var error = parseInt(error.response.status);
        res.status(error).send(error.response);
      }else{
        res.status(404).send(error);
      }
      console.log('****************************************************');
    })
});

app.post('/credentials', function (req, res) {

  var rfc = req.body.rfc || '';
  var ciec = req.body.ciec || '';
  axios.post(
    `${apiUrl}credentials`,{
      "id_site": idSite,
	    "credentials": {
        "username": rfc,
        "password": ciec
      }
    },{
      headers: headersBearer
    })
    .then((response) => {
      res.send(response.data);
    })
    .catch((error) => {
      console.log('****************************************************');
      console.log('ERROR');
      if(error && error.response && error.response.status){
        console.log(error.response.status);
        var error = parseInt(error.response.status);
        res.status(error).send(error.response);
      }else{
        res.status(404).send(error);
      }
      console.log('****************************************************');
    })
});

app.get('/jobs/:jobId/status', (req, res) => {
  console.log(`${apiUrl}jobs/${req.params.jobId}/status`);

  axios.get(
    `${apiUrl}jobs/${req.params.jobId}/status`,{
      headers: headersBearer
    })
    .then((responseSuc) => {
      console.log('****************************************************');
      console.log('SUCCSESS');
      console.log(responseSuc)
      res.send(responseSuc.data);
      console.log('****************************************************');
    })
    .catch((error) => {
      console.log('****************************************************');
      console.log('ERROR');
      if(error && error.response && error.response.status){
        console.log(error.response.status);
        var error = parseInt(error.response.status);
        res.status(error).send(error.response);
      }else{
        res.status(404).send(error);
      }
      console.log('****************************************************');
    })
});
app.get('/transactions', function (req, res) {
  var params = req.query;
  console.log(params);

  axios.get(
    `${apiUrl}transactions`,{
      params,
      headers: headersBearer
    })
    .then((responseSuc) => {
      console.log('****************************************************');
      console.log('SUCCSESS');
      console.log(responseSuc)
      res.send(responseSuc.data);
      console.log('****************************************************');
    })
    .catch((error) => {
      console.log('****************************************************');
      console.log('ERROR');
      if(error && error.response && error.response.status){
        console.log(error.response.status);
        var error = parseInt(error.response.status);
        res.status(error).send(error.response);
      }else{
        res.status(404).send(error);
      }
      console.log('****************************************************');
    })
});

app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});
